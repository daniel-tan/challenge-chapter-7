var express = require("express");
var router = express.Router();
const fs = require("fs");
const auth_path = "./auth.json";
const Model = require("../models");
const { userGame } = Model;

/* GET home page. */
router.get("/", (req, res) => {
	res.render("index", {
		message: "welcome",
		status: "success",
	});
});

router.post("/login-admin", (req, res) => {
	const { id, pass } = req.body;
	fs.readFile(auth_path, "utf8", (err, data) => {
		if (err) {
			res.render("index", {
				message: "error , " + err.message,
				status: "danger",
			});
		} else {
			const auth = JSON.parse(data);
			if (auth.id === id && auth.password === pass) {
				req.session.loggedIn = true;
				req.session.status = "super admin";
				res.redirect("admin");
			} else {
				res.render("index", {
					message: "Wrong id or password please try again",
					status: "danger",
				});
			}
		}
	});
});

router.post("/login", (req, res) => {
	const { userid, password } = req.body;
	userGame
		.findOne({
			userid: userid,
		})
		.then((user) => {
			if (user) {
				res.render("login", {
					message: "Wrong id or password please try again",
					status: "danger",
				});
			} else {
				userGame.findOne({ userid: userid, password: password })
				.then((cekPass) => {
					const payload = { userid: userid };
					jwt.sign(payload, "this is V3rY! 53crEt!1!", {
						expires: "1h",
					});
				})
			}
		})
		.catch((err) => {
			res.render("login", {
				message: err,
				status: "danger",
			});
			console.log("error while user login : ", err);
		});
});

router.get("/logout", (req, res) => {
	req.session.destroy();
	res.render("index", {
		message: "You have been logged out",
		status: "success",
	});
});

module.exports = router;
