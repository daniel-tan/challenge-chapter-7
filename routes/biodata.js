var express = require("express");
const session = require("express-session");
var router = express.Router();
const Model = require("../models");
const { userGame, userGameBiodata } = Model;

// * middleware override method
router.use((req, res, next) => {
	if (req.query._method === "DELETE") {
		req.method = "DELETE";
		req.url = req.path;
	} else if (req.query._method === "PUT") {
		req.method = "PUT";
		req.url = req.path;
	}
	next();
});

// * middleware cek login
router.use((req, res, next) => {
	if (req.session.loggedIn == false) {
		res.render(index, {
			message: req.flash("Please login before access!"),
		});
	}
});

router.get("/", (req, res) => {
	userGameBiodata
		.findAll(
			{
				order: ["userId"],
			},
			{
				include: [
					{
						models: userGame,
						attributes: ["userId", "username"],
					},
				],
			}
		)
		.then((data) => {
			console.log(data);
			res.render("biodata", {
				data: data,
				message: "",
			});
			// console.log(data);
		})
		.catch((err) => {
			console.log("error get data : ", err);
			res.status(500).send({ error: err });
		});
});

router.post("/add", (req, res) => {
	userGameBiodata
		.create({
			nama: req.body.nama,
			umur: req.body.umur,
			alamat: req.body.alamat,
			userId: req.body.userId,
		})
		.then(() => {
			res.redirect("/biodata");
		})
		.catch((err) => {
			console.log("error add data : ", err);
			res.render("biodata", {
				message: "Error creating new data! Please try again",
			});
		});
});

router.get("/update/:id", (req, res) => {
	userGame
		.findOne({
			where: { userId: req.params.id },
		})
		.then((data) => {
			res.render("update", {
				data: data,
				message: "",
			});
		})
		.catch((err) => {
			console.log("error update user : ", err);
			res.render("admin", {
				message: "error get user data, please try again",
			});
		});
});

module.exports = router;
