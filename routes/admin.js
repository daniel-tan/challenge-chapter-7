var express = require("express");
var router = express.Router();
const Model = require("../models");
const { userGame, userGameBiodata } = Model;

/* GET users listing. */
router.use((req, res, next) => {
	if (req.query._method === "DELETE") {
		req.method = "DELETE";
		req.url = req.path;
	} else if (req.query._method === "PUT") {
		req.method = "PUT";
		req.url = req.path;
	}
	next();
});

// * middleware cek login
router.use((req, res, next) => {
	if (req.session.loggedIn == false) {
		res.render(index, {
			message: req.flash("Please login before access!"),
		});
	}
});

router.get("/", (req, res) => {
	userGame
		.findAll({
			order: ["userId"],
			include: [
				{
					model: userGameBiodata,
					as: "biodata",
					attributes: ["nama", "alamat", "umur"],
				},
			],
		})
		.then((data) => {
			// console.log("cek data masuk : ", data);
			res.render("admin", {
				data: data,
				message: req.flash("info"),
				status: req.flash("status"),
			});
		})
		.catch((err) => {
			console.log("error get data : ", err);
			res.render("admin", {
				message: "Failed get data! Please refresh!",
				status: "danger",
			});
		});
});

router.get("/update/:id", (req, res) => {
	userGame
		.findOne({
			where: { userId: req.params.id },
			include: [
				{
					model: userGameBiodata,
					as: "biodata",
					attributes: ["nama", "alamat", "umur"],
				},
			],
		})
		.then((data) => {
			// console.log("data : ", data.userId);
			res.render("update", {
				data: data,
				message: req.flash("info"),
				status: req.flash("status"),
			});
		})
		.catch((err) => {
			console.log("error update user : ", err);
			res.render("admin", {
				message: "error get user data, please try again",
				status: "danger",
			});
		});
});

router.post("/add", (req, res) => {
	userGame
		.create({
			userId: req.body.userId,
			password: req.body.pass,
		})
		.then(() => {
			userGameBiodata
				.create({
					userId: req.body.userId,
					nama: req.body.nama,
					umur: req.body.umur,
					alamat: req.body.alamat,
				})
				.then(() => {
					req.flash("info", "Succesfully created new user!");
					req.flash("status", "success");
					res.redirect("/admin");
				})
				.catch((err) => {
					console.log("error add data : ", err);
					res.render("admin", {
						message: "Error creating new user! Please try again",
						status: "danger",
					});
				});
		})
		.catch((err) => {
			console.log("error add data : ", err);
			res.render("admin", {
				message: "Error creating new user! Please try again",
				status: "danger",
			});
		});
});

router.put("/update", (req, res) => {
	userGame
		.update(
			{
				password: req.body.pass,
			},
			{
				where: {
					userId: req.body.userId,
				},
			}
		)
		.then(() => {
			userGameBiodata
				.update(
					{
						nama: req.body.nama,
						alamat: req.body.alamat,
						umur: req.body.umur,
					},
					{
						where: {
							userId: req.body.userId,
						},
					}
				)
				.then(() => {
					req.flash("info", "Succesfully updated user!");
					res.redirect(`/update`);
				})
				.catch((err) => {
					console.log("error update data : ", err);
					res.render("update", {
						message: "Error updating user data! Please try again",
						status: "danger",
					});
				});
		})
		.catch((err) => {
			console.log("error update data : ", err);
			res.render("update", {
				message: "Error updating user data! Please try again",
				status: "danger",
			});
		});
});

router.delete("/:id", (req, res) => {
	userGame
		.destroy({
			where: {
				userId: req.params.id,
			},
		})
		.then(() => {
			req.flash("info", "Succesfully deleted user!");
			req.flash("status", "success");
			res.redirect("/admin");
		})
		.catch((err) => {
			console.log("error delete data : ", err);
			res.render("admin", {
				message: "Error deleting user data, please try again",
				status: "danger",
			});
		});
});

module.exports = router;
