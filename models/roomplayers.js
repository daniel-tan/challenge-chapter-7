"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class roomPlayers extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ userGameHistory }) {
			// define association here
			roomPlayers.hasOne(userGameHistory, {
				foreignKey: "roomId",
			});
		}
	}
	roomPlayers.init(
		{
			name: DataTypes.STRING,
			ownerId: DataTypes.INTEGER,
			challengerId: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: "roomPlayers",
			underscored: true,
		}
	);
	return roomPlayers;
};
