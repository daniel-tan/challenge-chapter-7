"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class userGame extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ userGameBiodata, userGameHistory }) {
			// define association here
			userGame.hasOne(userGameBiodata, {
				foreignKey: "userId",
				as: "biodata",
			});
			userGame.hasMany(userGameHistory, {
				foreignKey: "userId",
			});
		}
	}
	userGame.init(
		{
			userId: {
				type: DataTypes.STRING,
				primaryKey: true,
				tableName: "user_id",
			},
			password: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "userGame",
			underscored: true,
		}
	);
	return userGame;
};
