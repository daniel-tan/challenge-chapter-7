"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
	class userGameBiodata extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate({ userGame }) {
			// define association here
			userGameBiodata.belongsTo(userGame, {
				foreignKey: "userId",
				as: "biodata",
			});
		}
	}
	userGameBiodata.init(
		{
			userId: {
				type: DataTypes.STRING,
				tableName: "user_id",
			},
			nama: DataTypes.STRING,
			umur: DataTypes.STRING,
			alamat: DataTypes.STRING,
		},
		{
			sequelize,
			modelName: "userGameBiodata",
			tableName: "userGameBiodatum",
			underscored: true,
		}
	);
	return userGameBiodata;
};
