"use strict";
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable(
			"userGameHistories",
			{
				historyId: {
					primaryKey: true,
					type: Sequelize.INTEGER,
					autoIncrement: true,
				},
				userId: {
					allowNull: false,
					references: {
						model: {
							tableName: "user_games",
						},
						key: "user_Id",
					},
					type: Sequelize.STRING,
				},
				result: {
					type: Sequelize.STRING,
				},
				createdAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updatedAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			},
			{
				underscored: true,
			}
		);
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("userGameHistories");
	},
};
