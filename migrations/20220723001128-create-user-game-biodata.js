"use strict";
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable(
			"userGameBiodata",
			{
				userId: {
					allowNull: false,
					type: Sequelize.STRING,
					references: { model: "usergame", key: "userId" },
				},
				nama: {
					type: Sequelize.STRING,
				},
				umur: {
					type: Sequelize.STRING,
				},
				alamat: {
					type: Sequelize.STRING,
				},
				createdAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updatedAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			},
			{
				underscored: true,
			}
		);
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("userGameBiodata");
	},
};
