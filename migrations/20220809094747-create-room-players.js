"use strict";
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable("roomPlayers", {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			name: {
				type: Sequelize.STRING,
			},
			ownerId: {
				type: Sequelize.INTEGER,
			},
			challengerId: {
				type: Sequelize.INTEGER,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE,
			},
		});
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("roomPlayers");
	},
};
