"use strict";
module.exports = {
	async up(queryInterface, Sequelize) {
		await queryInterface.createTable(
			"userGames",
			{
				userId: {
					allowNull: false,
					primaryKey: true,
					type: Sequelize.STRING,
				},
				password: {
					type: Sequelize.STRING,
				},
				createdAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
				updatedAt: {
					allowNull: false,
					type: Sequelize.DATE,
				},
			},
			{
				underscored: true,
			}
		);
	},
	async down(queryInterface, Sequelize) {
		await queryInterface.dropTable("userGames");
	},
};
