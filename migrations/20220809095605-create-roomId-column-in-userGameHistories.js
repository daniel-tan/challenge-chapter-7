"use strict";

const { QueryInterface } = require("sequelize/types");

module.exports = {
	async up(queryInterface, Sequelize) {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.addColumn("userGameHistory", "room_Id", {
			type: Sequelize.INTEGER,
			reference: {
				model: {
					tableName: "room_players",
				},
				key: "room_Id",
			},
		});
	},

	async down(queryInterface, Sequelize) {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
	},
};
